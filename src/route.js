import { createRouter, createWebHistory } from 'vue-router'
// 0. If using a module system (e.g. via vue-cli), import Vue and VueRouter
// and then call `Vue.use(VueRouter)`.

// 1. Define route components.
// These can be imported from other files

import Reactivity from './components/reactivity.vue'
import Composition from './components/composition.vue'
import Users from './components/user-list.vue'
import Home from './components/home.vue'
import Mixin from './components/mixin.vue'
import CustomDirective from './components/custom-directive.vue'
import Teleport from './components/teleport.vue'
import ComputedWatch from './components/computed-watch.vue'
import WatchReactive from './components/watch-reactive.vue'
import Todos from './components/todo.vue'
import StateManagement from './components/state-management.vue'
import RenderFunction from './components/render-function.vue'
import Slot from './components/slot.vue'
// import ChangeDetection from './components/change-detection.vue'

// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.
// We'll talk about nested routes later.
const routes = [
    { path: '/', component: Home },
    { path: '/reactivity', component: Reactivity },
    { path: '/composition', component: Composition },
    { path: '/users', component: Users },
    { path: '/mixin', component: Mixin },
    { path: '/custom-directive', component: CustomDirective },
    { path: '/teleport', component: Teleport },
    { path: '/computed-watch', component: ComputedWatch },
    { path: '/watch-reactive', component: WatchReactive },
    { path: '/todos', component: Todos },
    { path: '/render-function', component: RenderFunction },
    { path: '/state-management', component: StateManagement },
    // { path: '/slot', component: Slot },
    { path: '/provide-inject', component: Slot }
]

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's
// keep it simple for now.
export const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes // short for `routes: routes`
})